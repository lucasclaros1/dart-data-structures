class Queue<T> {
  final List<T> list = [];

  void add(T value) => list.add(value);

  T? remove() => list.isNotEmpty ? list.removeAt(0) : null;
  T? top() => list.isNotEmpty ? list.first : null;
  int size() => list.length;
  void printQ() => list.forEach(print);
}
