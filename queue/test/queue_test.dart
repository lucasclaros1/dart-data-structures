import 'package:queue/queue.dart';
import 'package:test/test.dart';

void main() {
  var queue = Queue();
  group('Validating add/remove actions', () {
    test('Adding Elements', () {
      queue.add(5);
      expect(queue.top(), 5);
      queue.add(7);
      expect(queue.top(), 5);
      queue.add(10);
      expect(queue.top(), 5);
    });
    test('Removing Elements', () {
      queue.remove();
      expect(queue.top(), 7);
      queue.remove();
      expect(queue.top(), 10);
      queue.remove();
      expect(queue.top(), null);
    });
  });
}
