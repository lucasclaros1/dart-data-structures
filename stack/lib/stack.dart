class Stack<T> {
  final List<T> _stack = [];
  void push(T value) => _stack.add(value);
  T? top() => _stack.isNotEmpty ? _stack.last : null;
  T? pop() => _stack.isNotEmpty ? _stack.removeLast() : null;
}
