import 'package:stack/stack.dart';
import 'package:test/test.dart';

void main() {
  var stack = Stack();
  group('Validating push/pop actions', () {
    test('Pushing Elements', () {
      stack.push(5);
      expect(stack.top(), 5);
      stack.push(7);
      expect(stack.top(), 7);
      stack.push(10);
      expect(stack.top(), 10);
    });
    test('Poping Elements', () {
      stack.pop();
      expect(stack.top(), 7);
      stack.pop();
      expect(stack.top(), 5);
      stack.pop();
      expect(stack.top(), null);
    });
  });
}
